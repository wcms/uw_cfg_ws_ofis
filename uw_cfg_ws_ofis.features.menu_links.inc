<?php

/**
 * @file
 * uw_cfg_ws_ofis.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_cfg_ws_ofis_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link:
  // menu-site-management_ofis-settings:admin/config/system/uw_ws_ofis_settings.
  $menu_links['menu-site-management_ofis-settings:admin/config/system/uw_ws_ofis_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_ws_ofis_settings',
    'router_path' => 'admin/config/system',
    'link_title' => 'OFIS settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_ofis-settings:admin/config/system/uw_ws_ofis_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -30,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('OFIS settings');

  return $menu_links;
}
