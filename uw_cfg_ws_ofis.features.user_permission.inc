<?php

/**
 * @file
 * uw_cfg_ws_ofis.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_ws_ofis_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer OFIS configuration'.
  $permissions['administer OFIS configuration'] = array(
    'name' => 'administer OFIS configuration',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_cfg_ws_ofis',
  );

  return $permissions;
}
